#include "pm.h"
#include <signal.h>
#include "mproc.h"

PUBLIC int do_wfnp()
{
	int i = 0;

	printf("\n");
	for(i = 0; i < sizeof(mp->mp_name); i++){
		printf("%c", mp->mp_name[i]);
	}

	if(1 == mp->mp_realtime_enabled){
		printf(" is waiting for the next period\n");
		return(OK);
	} else {
		printf(" is not a realtime job!!!\n");
		return(-1);
	}
}
