#include "syslib.h"

PUBLIC int sys_wfnp(endpoint_t proc_ep)
{
	message m;
	m.m_source = proc_ep;
	m.REATIME_WFNP = 1;

	return _kernel_call(SYS_WFNP, &m);
}
