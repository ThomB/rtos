#include "syslib.h"

PUBLIC int sys_setrt(endpoint_t proc_ep, int realtime, int startTime, int period, int computationTime, int deadline){
    message m;
    m.m_source = proc_ep;
    m.REALTIME_ENABLED = realtime;
    m.REALTIME_STARTTIME = startTime;
    m.REALTIME_PERIOD = period;
    m.REALTIME_COMPUTATIONTIME = computationTime;
    m.REALTIME_DEADLINE = deadline;

	  printf("realtime in the clock, period: %d\n", period);
    return _kernel_call(SYS_SETRT, &m);
}
