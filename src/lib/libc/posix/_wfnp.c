#include <lib.h>
#define wfnp	_wfnp
#include <sys/wfnp.h>


PUBLIC int wfnp()
{
	message m;

	return (_syscall(PM_PROC_NR, WFNP, &m));
}
