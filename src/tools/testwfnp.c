#include <lib.h>
#include <stdio.h>
#include <stdlib.h>
#include <realtime.h>

void main(int argc, char *argv[])
{
  int i;
  message m;

  /* Method one */
  i = wfnp();
  /* Method two */
  i = _syscall(PM_PROC_NR, WFNP, NULL);
  return;
}
