#include <lib.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <minix/callnr.h>
#include <realtime.h>

void main(int argc, char *argv[])
{
	message m;
	int i = 0, j = 0, k = 0;

	do
	{
		i++;
		for(j = 0; j < 1000000; j++)
		{
			k += (k % 100) / 20 + 2;
		}
	} while((_syscall(PM_PROC_NR, WFNP, &m) == 0 && i < 2));
}
