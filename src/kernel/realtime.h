/* The <realtime.h> header defines the structs and funcions for realtime scheduling. */
#ifndef _REALTIME_H
#define _REALTIME_H
typedef struct realtimeData{
	int startTime;
	int period;
	int calcTime;
	int deadline;
  int wfnp;
  int enabled;

	int period_distance;
	int deadline_distance;
} realtimeData;


#endif /* _REALTIME_H */
