#include "kernel/system.h"
#include <minix/type.h>
#include "kernel/proc.h"
/*===========================================================================*
 *				do_setrt									     *
 *===========================================================================*/
PUBLIC int do_setrt(struct proc * caller, message * m_ptr){
	/*Source: do_vmctl*/
	int proc_nr;
  endpoint_t ep = m_ptr->m_source;
  struct proc *p;

	if(!isokendpt(ep, &proc_nr)) {
		printf("do_setrt: unexpected endpoint %d\n", ep);
		return EINVAL;
	}

	p = proc_addr(proc_nr);

	p->rtdata.startTime        = m_ptr->REALTIME_STARTTIME;
	p->rtdata.period           = m_ptr->REALTIME_PERIOD;
	p->rtdata.period_distance  = m_ptr->REALTIME_PERIOD;
	p->rtdata.calcTime         = m_ptr->REALTIME_COMPUTATIONTIME;
	p->rtdata.deadline         = m_ptr->REALTIME_DEADLINE;
	p->rtdata.deadline_distance = m_ptr->REALTIME_DEADLINE;
	p->rtdata.enabled          = 1;

	remove_from_queue(STD_QUEUE, p);
	remove_from_queue(REALTIME_QUEUE, p);
	add_to_queue(WFNP_QUEUE, p);

	return 1;
}
