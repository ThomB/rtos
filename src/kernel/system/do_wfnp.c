#include "kernel/system.h"
#include <minix/type.h>
#include "kernel/proc.h"
/*===========================================================================*
 *				do_wfnp									     *
 *===========================================================================*/
PUBLIC int do_wfnp(struct proc * caller, message * m_ptr){
	/*Source: do_vmctl*/
	int proc_nr;
  endpoint_t ep = m_ptr->m_source;
  struct proc *p;

	if(!isokendpt(ep, &proc_nr)) {
		printf("do_setrt: unexpected endpoint %d\n", ep);
		return EINVAL;
	}

	p = proc_addr(proc_nr);
	p->rtdata.deadline_distance = p->rtdata.deadline;

	/*TODO this is not really correct, periods are absolute blocks not dependent on when they are ready running? */
	p->rtdata.period_distance = p->rtdata.period;

	remove_from_queue(STD_QUEUE, p);
	remove_from_queue(REALTIME_QUEUE, p);
	add_to_queue(WFNP_QUEUE, p);

  return 1;
}
