/* The <realtime.h> header defines the structs and funcions for realtime scheduling. */
#ifndef _REALTIME_H
#define _REALTIME_H
typedef struct realtimeData{
	int startTime;
	int period;
	int calcTime;
	int deadline;
} realtimeData;

_PROTOTYPE(int wfnp, (void));

#endif /* _REALTIME_H */
